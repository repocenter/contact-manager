package main

import(
	"encoding/json"
	"net/http"
)

func JsonContactDeploy(c Contact, res http.ResponseWriter){
	sendContact := c
	result, _ := json.Marshal(sendContact)
	res.Write(result)
}

func JsonListDeploy(c []Contact, res http.ResponseWriter){
	sendContact := c
	result, _ := json.Marshal(sendContact)
	res.Write(result)
}

