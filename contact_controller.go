package main

///////////
//imports//
///////////

import(
	"net/http"
	"encoding/json"
	"strconv"
	"fmt"
)

////////////////////////////////////////
//map that leads to the right function//
////////////////////////////////////////

var MethodMap = map[string]func(res http.ResponseWriter, req *http.Request){
	"GET":ContactGET,
	"POST":ContactPOST,
	"PUT":ContactPUT,
}

//////////////////////////////////////////////
//func used to get right function of the map//
//////////////////////////////////////////////

func HandleContact(res http.ResponseWriter, req *http.Request){
	method := req.Method
	f := MethodMap[method]
	f(res,req)
}

///////////////////////
//get function logics//
///////////////////////

func ContactGET(res http.ResponseWriter, req *http.Request){
	name := req.URL.Query().Get("name")
	if name != ""{
		user, err := SearchByName(name, contactList)
		if err == nil{
			JsonContactDeploy(user,res)
			return
		}
		res.Write([]byte("User not found"))
		return
	}
	if len(contactList) == 0{
		res.Write([]byte("You don`t have any contacts yet"))
		return
	}
}

////////////////////////
//post function logics//
////////////////////////

var contactList []Contact
func ContactPOST(res http.ResponseWriter, req *http.Request){
	list, err := Insert(req.Body)
	if err != nil{
		fmt.Println(err)
		res.Write([]byte("Error"))
		return
	}
	contactList = list
	//result, _ := json.Marshal(list)
	//res.Write(result)
	res.Write([]byte("Contact register succesfuly!"))
}

///////////////////////
//put function logics//
///////////////////////

func ContactPUT(res http.ResponseWriter, req *http.Request){
	id, _ := strconv.Atoi(req.URL.Query().Get("ip"))
	user, _ := SearchById(id, contactList)
	//get req body for later use
	var tempUser Contact
	err := json.NewDecoder(req.Body).Decode(&tempUser)
	if err != nil{
		res.Write([]byte("Error during JSON Decoding"))//TODO error handle
		return
	}
	//change the non nil values
	if tempUser.Name  != ""{
		user.Name = tempUser.Name
	}
	if tempUser.LastName  != ""{
		user.LastName = tempUser.LastName
	}	
	if tempUser.Phones != nil{
		user.Phones = tempUser.Phones
	}
}
