package main

/////////////////////
//contact structure//
/////////////////////

type Contact struct{
	Id int	    `json:"id"`
	Name string `json:"name"`
	LastName string `json:"last_name"`
	Phones []Phone `json:"phones"`
}

//////////////////////////
//phone number structure//
//////////////////////////

type Phone struct{
	CC string `json:"cc"`
	DDD string `json:"ddd"`
	Number string `json:"number"`
}
