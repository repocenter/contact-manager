package main

import(
	"net/http"
	"log"
)

///////////////////////////////////////////
//func that handle with service structure//
///////////////////////////////////////////

func RunService(){
	 http.HandleFunc("/contact", HandleContact)
	 log.Fatal(http.ListenAndServe("127.0.0.1:8080", nil))
}
