package main

import ("math/rand/v2"
	"errors"
	"fmt"
	"io"
	"encoding/json"
)

/////////////////////
//ID cration logics//
/////////////////////

var Ids []int

func CreateId()int{
	var idExists bool = false
	var id int
	for {
		id = rand.IntN(1000)
		for i := 0; i < len(Ids);i++{
			if Ids[i] == id{
				idExists = true
				break
			}
			idExists = false
		}
		if !idExists{
			Ids = append(Ids,id)
			break
		}
	}
	return id
}

/////////////////////////////////////
//func that find a user by his name//
/////////////////////////////////////

func SearchByName(name string, contactList []Contact)(Contact, error){
	for i := 0; i < len(contactList);i++{
		if contactList[i].Name == name{
			return contactList[i], nil
		}
		fmt.Println("ID nao encontrado")
		}
		return Contact{} , errors.New("ID não encontrado")
}

///////////////////////////////////
//func that find a user by his ID//
///////////////////////////////////

func SearchById(id int, contactList []Contact)(*Contact, error){
	for i := 0; i < len(contactList);i++{
		if contactList[i].Id == id{
			return &contactList[i], nil
		}
		fmt.Println("ID nao encontrado")
		}
		return &Contact{} , errors.New("ID não encontrado")
}

/////////////////////////////////////////////
//post function to insert users in database//
/////////////////////////////////////////////


func Insert(body io.ReadCloser)([]Contact, error){
	var tempUser Contact
	err := json.NewDecoder(body).Decode(&tempUser)
	if err != nil{
		return nil,err
	}
	tempUser.Id = CreateId()
	
	contactList = append(contactList, tempUser)
	
	return contactList, err
}
